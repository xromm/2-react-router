import React from 'react';

import { Link } from 'react-router';

export default class Layout extends React.Component {
  navigate() {
    //console.log(this.props); //наследует много всего, route,history...
    //this.props.history.pushState(null, "/"); // будет работать кнопка бэк на предыдущую страницу
    this.props.history.replaceState(null, "/"); // не будет работать кнопка бэк на предыдущую страницу
  }

  render() {
    return (
      <div>
        <h1>Hello my friend!</h1>
        {this.props.children}
        <Link to="archives" class="btn btn-danger">archives</Link>
        <Link to="settings"><button class="btn btn-success">settingss</button></Link>
        <button onClick={this.navigate.bind(this)}>featured</button>
      </div>
    );
  }
}
